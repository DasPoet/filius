# Filius - Simple Network Simulation
Filius is a network simulator for educational purpose

## Licencse
Filius is provided under the terms of GPLv2 or GPLv3.

## Requirements
Filius is a Java application. It is required that there is a Java Runtime Environment 8 or higher. Support of Java 8 is deprecated and will end soon.

For most features, JRE will do. The feature to implement and run your own applications (by means of the Software Wizard) requires Java Development Kit.

## OS Support
Windows, Linux

## Available Languages
German, French, English

## Configuration
The personal configuration, i.e. selected language as well as the ui state when leaving the program, is stored within the personal home directory within a folder named ".filius".

Global configurations can be modified in the install directory within "filius.ini". For more details see the documented parameter in this file.

## Installation
The Windows installer is based on nsis. It supports the standard parameter:
* /S - for silent install/uninstall
* /D - to set the install directory (e.g. /D=Z:\Filius)

## Web site
https://www.lernsoftware-filius.de/